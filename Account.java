public abstract class Account {

    static double interestRate;
    private double balance;
    private String name;

    public Account(String name, double balance )
    {
        setName(name);
        setBalance(balance);
    }

    public Account()
    {
        this("Jordan", 50);

    }

    public boolean withdraw(double amount)
    {
        return amount < this.balance;
    }

    public boolean withdraw()
    {
        this.balance -= 20;
        return withdraw(20);
    }

    public abstract void addInterest();


    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }
    
}