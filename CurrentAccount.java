public class CurrentAccount extends Account{
    
    CurrentAccount(String name, double balance)
    {
        super(name, balance);
    }

    @Override
    public void addInterest() {
        setBalance(getBalance() * 1.1);
    }


}