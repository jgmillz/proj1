public class TestInheritance {
    public static void main(String[] args) {
        
        Account[] AccountArray = new Account[2];
        // AccountArray[0] = new Account("Bob", 2);
        AccountArray[0] = new SavingsAccount("Jordan", 4);
        AccountArray[1] = new CurrentAccount("Pete", 6);

        for(Account a:AccountArray)
        {
            System.out.println(a.getBalance());
            a.addInterest();
            System.out.println(a.getBalance());

        }

    }
    
}